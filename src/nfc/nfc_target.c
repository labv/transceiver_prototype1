#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <nfc/nfc.h>

#include "nfc-utils.h"
#include "ndef.h"
#include "nfc_target.h"
#include "utils.h"


/* local NFC variables */
static nfc_device *pnd;
static nfc_context *context;
static int targetInitialized = 0;
// Initialize NFC target properties
// The properties will be updated by the nfc_target_init()
nfc_target nt = {
  .nm = {
    .nmt = NMT_ISO14443A,
    .nbr = NBR_UNDEFINED, // Will be updated by nfc_target_init()
  },
  .nti = {
    .nai = {
      .abtAtqa = { 0x00, 0x04 },
      .abtUid = { 0x08, 0x00, 0xb0, 0x0b },
      .szUidLen = 4,
      .btSak = 0x20,
      .abtAts = { 0x75, 0x33, 0x92, 0x03 }, /* Not used by PN532 */
      .szAtsLen = 4,
    },
  },
};

/* Version of the emulated type4 tag */
static int type4v = 2;

/* NFC payload file types
 * CC = capability container */
static enum nfc_payload_type 
{ 
  NONE, 
  CC_FILE, 
  NDEF_FILE 
} nfc_state;

/* used for CC file (NDEF capability container) */
uint8_t nfcforum_capability_container[] = {
  0x00, 0x0F, /* CCLEN 15 bytes */
  0x20,       /* Mapping version 2.0, use option -1 to force v1.0 */
  0x00, 0x54, /* MLe Maximum R-ADPU data size */
// Notes:
//  - I (Romuald) don't know why Nokia 6212 Classic refuses the NDEF message if MLe is more than 0xFD (any suggests are welcome);
//  - ARYGON devices doesn't support extended frame sending, consequently these devices can't sent more than 0xFE bytes as APDU, so 0xFB APDU data bytes.
//  - I (Romuald) don't know why ARYGON device doesn't ACK when MLe > 0x54 (ARYGON frame length = 0xC2 (192 bytes))
  0x00, 0xFF, /* MLc Maximum C-ADPU data size */
  0x04,       /* T field of the NDEF File-Control TLV */
  0x06,       /* L field of the NDEF File-Control TLV */
  /* V field of the NDEF File-Control TLV */
  0xE1, 0x04, /* File identifier */
  0xFF, 0xFE, /* Maximum NDEF Size */
  0x00,       /* NDEF file read access condition */
  0xFF,       /* NDEF file write access condition */
};

/* C-ADPU offsets */
static const int CLA = 0;
static const int INS = 1;
static const int P1 = 2;
static const int P2 = 3;
static const int LC = 4;
static const int DATA = 5;


/**
 * Encapsulate dataRecords in an ndef message.
 * dataRecords is an array of char arrays (strings) to be supplied in; each array represents
 * one data record. The number of records should correspond to numRecords
 * The encapsulated data will be stored in outBuffer.
 * Return 0 if success, -1 if failed.
 */
static int buildNdef(char* outBuffer, int outLength, char** dataRecords, int numRecords) {
  // initialize the entire outBuffer to NUL
  memset(outBuffer, 0, outLength);
  
  // begin the process of transcribing records to outBuffer using the NDEF format
  int i = 2, recI = 0;
  for (recI=0; recI<numRecords; recI++) {
    int recordLength = strlen(dataRecords[recI]); // do not need null terminator

    // first and/or last record?
    bool isFirst = false;
    bool isLast = false;
    if (recI==0)
      isFirst = true;
    if (recI==numRecords-1)
      isLast = true;

    // parameters:
    // tnf, is_begin, is_end, is_chunk
    // is_short, has_length
    // type, type_length
    // id, id_length
    // payload, payload_length
    struct ndef_record* r = ndef_create(
      EXTERNAL, isFirst, isLast, false,
      true, false,
      "android.com:pkg", 15,
      NULL, 0,
      dataRecords[recI], recordLength
    );

    outBuffer[1] += r->length;

    // check to make sure the buffer has adequate size
    if (outLength <= 2 + outBuffer[i-1]) {
      return -1;
    }

    // copy payload to buffer
    memcpy(&outBuffer[i], r->buffer, r->length);
    i += r->length;

    // free memory
    ndef_destroy_buffer(r);
  }

  return 0;
}


/**
 * Based on the command received (C-APDU), decipher what response should be sent out.
 * Const string appName and msg are the transceiver specific data, they will be parsed
 * by the function as the appropriate response if appropriate command has been detected.
 *
 * Returns the number of bytes that should be transferred as response, and stores into responseOut
 * Returns -1 on failure
 * When termination_success = 1, the whole transaction is finished and should exit
 */
int nfcTargetDecipherIO(const uint8_t* data_in, const size_t data_in_len, uint8_t* data_out, size_t data_out_len, int* termination_success)
{
  // initialization
  int res = 0;
  *termination_success = 0;
  memset(data_out, 0, data_out_len);

  // No input data, nothing to do
  if (data_in_len == 0) {
    return 0;
  }

  if (data_in_len >= 4) {
    // C-APDU must begin with 0x00
    if (data_in[CLA] != 0x00)
      return -ENOTSUP;

#define ISO7816_SELECT 0xA4
#define ISO7816_READ_BINARY 0xB0
#define ISO7816_PRESENT_USER 0x14
#define ISO7816_UPDATE_BINARY 0xD6   // not used

    // decipher the instruction that came from the initiator
    switch (data_in[INS]) {
      case ISO7816_SELECT:
        // select is used to determine the capability of the target as well as to establish
        // uniform data exchange formats

        //- select begins
        switch (data_in[P1]) {
          case 0x00: /* Select by ID */
fprintf(stderr, "select by id\n");
            if ((data_in[P2] | 0x0C) != 0x0C)
              return -ENOTSUP;

            // determine which file identifier has been requested
            const uint8_t cc_request[] = { 0xE1, 0x03 };
            const uint8_t ndef_request[] = { 0xE1, 0x04 };    // defined in NDEF capability container
            if ((data_in[LC] == sizeof(cc_request)) && (0 == memcmp(cc_request, data_in + DATA, data_in[LC]))) {
              // capability container requested
              memcpy(data_out, "\x90\x00", res = 2);
              nfc_state = CC_FILE;
            } else if ((data_in[LC] == sizeof(ndef_request)) && (0 == memcmp(ndef_request, data_in + DATA, data_in[LC]))) {
              // NDEF encapsulated data requested
              memcpy(data_out, "\x90\x00", res = 2);
              nfc_state = NDEF_FILE;
            } else {
              // unknown data type requested, return "no info given"
              memcpy(data_out, "\x6a\x00", res = 2);
              nfc_state = NONE;
            }

            break;
          case 0x04: /* Select by name */
fprintf(stderr, "select by name\n");
            if (data_in[P2] != 0x00)
              return -ENOTSUP;

            const uint8_t ndef_tag_application_name_v1[] = { 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x00 };
            const uint8_t ndef_tag_application_name_v2[] = { 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01 };
            if ((type4v == 1) && (data_in[LC] == sizeof(ndef_tag_application_name_v1)) && (0 == memcmp(ndef_tag_application_name_v1, data_in + DATA, data_in[LC]))) {
              memcpy(data_out, "\x90\x00", res = 2);
            } else if ((type4v == 2) && (data_in[LC] == sizeof(ndef_tag_application_name_v2)) && (0 == memcmp(ndef_tag_application_name_v2, data_in + DATA, data_in[LC]))) {
              memcpy(data_out, "\x90\x00", res = 2);
            } else {
              // unknown tag application, return "file not found"
              memcpy(data_out, "\x6a\x82", res = 2);
            }

            break;
          default:
            return -ENOTSUP;
        }
fprintf(stderr, "NFC STATE = %d\n", nfc_state);
        break;
        //- select ends
      case ISO7816_READ_BINARY:
        // read requests the data to be returned from the target
fprintf(stderr,"READ\n");
        //- read begins

        // check for overflow
        if ((size_t)(data_in[LC] + 2) > data_out_len) {
          return -ENOSPC;
        }

        // from previous request, an NFC payload type was selected
        switch (nfc_state) {
          case NONE:
            // we have yet to receive request as to what data to send out
            memcpy(data_out, "\x6a\x82", res = 2);
            break;
          case CC_FILE:
            // request capability container to be sent out
            // P1P2 defines the offset
            memcpy(data_out, nfcforum_capability_container + (data_in[P1] << 8) + data_in[P2], data_in[LC]);
            memcpy(data_out + data_in[LC], "\x90\x00", 2);
            res = data_in[LC] + 2;
            
            break;
          case NDEF_FILE:
            // initiator requested NDEF data to be returned

            res = 0;
          
            // encapsulate payload in NDEF
            const size_t MAX_NDEF_LEN = 0xFFFE;    // this limit was defined in the NDEF capability container
            uint8_t ndef_file[MAX_NDEF_LEN];
            int i = 0; for (i=0; i<MAX_NDEF_LEN; i++) ndef_file[i] = NUL_CHAR;

            // AAR encoded Android app
            char *dataRecords[1];
            dataRecords[0] = ANDROID_APP_NAME;
            if (buildNdef((char*)ndef_file, MAX_NDEF_LEN, dataRecords, 1) < 0){
              fprintf(stderr, "Error building NDEF\n");
              return -1;
            }
            size_t ndef_file_len = ndef_file[1] + 2;
          
            // construct output buffer
            memcpy(data_out, ndef_file + (data_in[P1] << 8) + data_in[P2], data_in[LC]);
            memcpy(data_out + data_in[LC], "\x90\x00", 2);
            res = data_in[LC] + 2;
            
            break;
        }
        
        break;
        //- read ends

      case ISO7816_PRESENT_USER:
        //- user operation begins

        if (data_in[P1] != 0x00 && data_in[P2] != 0x80)
          return -ENOTSUP;

        // check length
        if (data_in[LC] > USER_ID_LEN)
          return -ENOTSUP;

        // copy user id
        memcpy(customer_user_id, data_in+DATA, data_in[LC]);
fprintf(stderr, "customer_user_id = %s\n\n\n", customer_user_id);

        // send confirmation
        *termination_success = 1;
        res = 2;
        memcpy(data_out, "\x90\x00", res);

        //- user operation ends
        break;

      default: // Unknown
        res = -ENOTSUP;
    }
  } else {
    res = -ENOTSUP;
  }

  return res;
}


/**
 * Wrapper function for receiving commands (C-APDU)
 * Returns the number of bytes received. Returns -1 on failure.
 * Stores the received command in data_in 
 */
int nfcTargetReceiveCommand(uint8_t* data_in, int timeout){
  int res = -1;

  if (!targetInitialized){
    // open NFC device to target mode
    int res = nfc_target_init(pnd, &nt, data_in, MAX_COMMAND_LEN, timeout);
    if (res >= 0)
      targetInitialized = 1;

if (res>=0){
fprintf(stderr, "\nINIT NEW RECEPTION:\n");
print_hex(data_in, res);
}
    return res;
  }

  res = nfc_target_receive_bytes(pnd, data_in, MAX_COMMAND_LEN, timeout);
  if (res == NFC_ETGRELEASED)
    targetInitialized = 0;

if (res>=0){
fprintf(stderr, "\nNEW RECEPTION:\n");
print_hex(data_in, res);
}
  return res;
}



/**
 * Wrapper function for emitting the response (R-APDU)
 * Returns 0 on success. Returns -1 on failure.
 */
int nfcTargetTransferResponse(uint8_t* data_out, size_t data_out_len, int timeout){
  int res = nfc_target_send_bytes(pnd, data_out, data_out_len, timeout);
  if (res == NFC_ETGRELEASED)
    targetInitialized = 0;
  return res;
}


/**
 * Initialize NFC
 * On success, return 0, otherwise, return -1
 */
int nfcTargetInit(){
  // initialize NFC state
  nfc_state = NONE;

  // clear user_id
  memset(customer_user_id, 0, USER_ID_LEN);

  // Sometimes the initialization may failed
  // Continue trying a few times
  int tries = 0;
  const int totalTries = 5;
  for (tries=0; tries<totalTries; tries++){
    // initialize libnfc
    nfc_init(&context);
    if (context == NULL){
      fprintf(stderr, "Unable to init libnfc");
      continue;
    }

    // open NFC device
    // should supply the connstring for PN532
    pnd = nfc_open(context, NULL);
    if (pnd == NULL){
      fprintf(stderr, "Unable to open NFC device");
      nfc_exit(context);
      continue;
    }

    // disable infinite time target selection
    if (nfc_device_set_property_bool(pnd, NP_INFINITE_SELECT, false) < 0){
      fprintf(stderr, "Unable to customize nfc_device");
      nfcTargetClose();
      continue;
    }

    break; // do not allow the last tries++
  }

  if (tries >= totalTries){
    targetInitialized = 0;
    return -1;
  } else {
    return 0;
  }
}


/**
 * Close NFC
 */
void nfcTargetClose(){
  if (pnd != NULL){
    nfc_abort_command(pnd);
    nfc_close(pnd);
  }
  if (context != NULL)
    nfc_exit(context);

  targetInitialized = 0;
}

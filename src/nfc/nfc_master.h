#ifndef __NFC_APP__
#define __NFC_APP__

#include "ndef.h"

/**
 * Initialize NFC
 * On success, return 1, otherwise, return 0
 */
int nfcMasterInit();

/**
 * Return 1 if an NFC target is present
 * Return 0 otherwise
 * Stores the discovered NFC target in nt
 */
int nfcDiscoverTarget();

/**
 * Transmit then receive data from the target
 * Return 1 on success, return 0 on failure
 * The data to be transmitted should be specified in txData
 * The returned NDEF encapsulated message will be stored in rxData
 */
int nfcMasterTransceive(char** txData, int numRecordsTx, struct ndef_message* rxData, int timeout);

/* 
 * Close NFC
 */
void nfcMasterClose();

#endif

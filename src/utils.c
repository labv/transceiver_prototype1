#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "utils.h"

/**
 * Free memory of a dynamic array
 */
void deleteArray(char** arrayPtr){
  free(*arrayPtr);
  arrayPtr = NULL;
}

/**
 * Increase size of memory_struct by certain amount
 * Return 1 on success, 0 on failure
 */
int increaseMemoryStruct(struct memory_struct* ms, size_t size){
  size_t newSize = ms->size + size;

  if (ms->memory == NULL){
    ms->memory = (char*)malloc(newSize);

    if (ms->memory != NULL){
      ms->size = newSize;
      return 1;
    } else {
      return 0;
    }
  } else {
    ms->memory = (char*)realloc(ms->memory, newSize);
    if (ms->memory != NULL){
      ms->size = newSize;
      return 1;
    } else {
      return 0;
    }
  }
}


/**
 * Delete memory_struct
 */
void deleteMemoryStruct(struct memory_struct* ms){
  if (ms->memory != NULL){
    deleteArray(&(ms->memory));
  }
  ms->memory = NULL;
  ms->size = 0;
  ms->used_size = 0;
}


/**
 * Trim string
 */
void strtrim(const char* orig_str, char* out, const size_t out_len){
  memset(out, 0x00, out_len);

  int i = 0;
  // find the first non-whitespace character
  for (i=0; (orig_str[i]==' ' || orig_str[i]=='\n') && orig_str[i]!='\0'; i++);

  int j = 0;
  // find the last non-whitespace character
  // first, advance to terminator
  j = strlen(orig_str);
  if (j==0 || j==i)
    return;
  // go back, stop at the last non-whitespace char
  for (j--; orig_str[j]==' ' || orig_str[j]=='\n'; j--);

  // copy chars to out
  int k = 0, counter = 0;
  for (k=i; k<=j; k++)
    out[counter++] = orig_str[k];

  // append termination
  out[counter] = '\0';
}


/**
 * Remove dash from the string
 */
void strRemoveDash(const char* orig_str, char* out, const size_t out_len){
  memset(out, 0x00, out_len);

  int len = strlen(orig_str);
  int i=0, j=0;
  for (i=0; i<len && j<out_len-1; i++){
    if (orig_str[i] != '-')
      out[j++] = orig_str[i];
  }

  out[j] = '\0';
}


/**
 * Get elapsed time in ms since start_time
 */
int getElapsedTime(struct timespec start){
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  int ms_diff = (now.tv_nsec - start.tv_nsec)/1000000;

  return (now.tv_sec - start.tv_sec)*1000 + ms_diff;
}
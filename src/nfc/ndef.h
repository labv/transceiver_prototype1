#ifndef __NDEF_NDEF_H__
#define __NDEF_NDEF_H__

#define NO_ID 0

#define NDEF_PAYLOAD_TYPE_TEXT "T"
#define NDEF_PAYLOAD_TYPE_URI  "U"

#include <stdbool.h>
#include <stdint.h>

enum ndef_record_type {
  EMPTY = 0,
  WELL_KNOWN = 1,
  MIME = 2,
  URI = 3,
  EXTERNAL = 4
};

// an NDEF message is composed to one or several records
struct ndef_message {
    size_t length;
    struct ndef_record* records;
};

// A zero-copy data structure for storing ndef records. All fields should be
// accessed using accessors defined below.
struct ndef_record {
    char* buffer;
    size_t length;

    uint8_t type_length;
    size_t type_offset;

    uint8_t id_length;
    size_t id_offset;

    uint32_t payload_length;
    size_t payload_offset;
};

// retrieve an ndef record from a char buffer
struct ndef_record* ndef_parse(char* buffer, size_t offset);

// create a new ndef record from scratch
struct ndef_record* ndef_create(
        enum ndef_record_type tnf, bool is_begin, bool is_end, bool is_chunk,
        bool is_short, bool has_length,
        char* type, uint8_t type_length,
        char* id, uint8_t id_length,
        char* payload, uint32_t payload_length);

// used to free memory
struct ndef_record* ndef_destroy_buffer(struct ndef_record*);
struct ndef_record* ndef_destroy(struct ndef_record*);

// ndef record attribute accessors
uint8_t ndef_tnf(struct ndef_record*);
bool ndef_is_message_begin(struct ndef_record*);
bool ndef_is_message_end(struct ndef_record*);
bool ndef_is_chunk(struct ndef_record*);
bool ndef_is_short_record(struct ndef_record*);
bool ndef_has_id_length(struct ndef_record*);

#endif

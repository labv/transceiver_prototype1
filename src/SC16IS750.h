#ifndef _SC16IS750_H
#define _SC16IS750_H

#include <stdlib.h>

/**
 * Initialize the board
 * SPI = 0 or 1
 * Return 0 on success, -1 on failure
 */
int spi2uart_init(int SPI);

/**
 * End the spi2uart and return settings to default
 */
void spi2uart_end();

/**
 * Transfer data to uart
 * Will block until the len amount of bytes have been transferred, unless
 * connection has been severed, or idle time has surpassed TIMEOUT
 * Return the total number of bytes transferred
 */
size_t spi2uart_write(char* tx, const size_t len, const unsigned int timeout);

/**
 * Read data from uart
 * Will block until len bytes have been read or no more bytes to read
 * Return the number of bytes read
 */
size_t spi2uart_read(char* rx, const size_t len);

/**
 * Reset and flush the fifo
 */
void spi2uart_reset();

#endif

#ifndef _ESC_POS
#define _ESC_POS

#include "utils.h"


/* Type of printer communication channel */
enum escpos_comm_channel{
  UART = 0,
  SPI0 = 1,
  SPI1 = 2
};


// The following functions are used to communicate with the printer
// BEGIN

/**
 * Connect with the printer 
 * Return: -1 = failure, 0 = connection succeeded
 */
int escpos_printer_connect(enum escpos_comm_channel comm, size_t timeout);

/**
 * Get status of the printer 
 */
int escpos_printer_status(enum escpos_comm_channel comm, size_t timeout);

/**
 * Transfer texts or graphics to the printer 
 * Return: -1 = failure, 0 = success
 */
int escpos_printer_transfer(enum escpos_comm_channel comm, struct memory_struct *data, size_t timeout);

// END printer specific functions


// The following functions are used to communicate with the PC
// BEGIN

/**
 * Respond to commands from PC
 * Will store useful receipt data into the data pointer
 * Return: -1 = failed to communicate, 0 = good communication, 1 = successful exit
 */
int escpos_pc_communicate(enum escpos_comm_channel comm, struct memory_struct *outData);

// END PC specific functions

#endif

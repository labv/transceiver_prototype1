#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

// needed for delay
#include <bcm2835.h>

// UART
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

// SPI to UART bridge
#include "SC16IS750.h"

#include "escpos.h"

// ESC/POS command codes
#define DLE 0x10
#define ESC 0x1b
#define FS  0x1c
#define GS  0x1d
#define EOT 0x04
#define ENQ 0x05
#define DC4 0x14
#define TAB 0x09
#define LF  0x10
#define SPACE 0x32

// command state
// This enum tells us which command it's in
enum COMMAND_STATE {
  REALTIME,   // realtime command, requires tx
  RECOGNIZED, // recognized command, but still requires further data for deciphering
  UNDECIDED,  // not sure yet, more info (bytes) required
  PASS_THRU,  // pass buffer to the output + certain ensuing bytes to the output
  IGNORE,     // throw away the buffer
};

// for UART port
static int uart_stream = -1;
// PC connected status
static int pc_connected = -1;


/**
 * Setup uart
 * Return the filestream read from the uart port
 * If no stream present, return -1
 */
int openUart(){
  int uart0_filestream = -1;

  // O_RDWR = read write
  // O_NDELAY = non blocking mode
  // O_NOCTTY = no controlling terminal enabled
  uart0_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
  if (uart0_filestream == -1){
    return uart0_filestream;
  }

  // configure the uart
  struct termios options;
  tcgetattr(uart0_filestream, &options);
  // B38400 = baud rate
  // CS8 = data size
  // CLOCAL = ignore modem status lines
  // CREAD = enable receiver
  // PARENB = enable parity checking, default to even
  // IGNPAR = ignore characters with parity errors
  options.c_cflag = B38400 | CS8 | CLOCAL | CREAD | PARENB;
  options.c_iflag = 0;//IXOFF | IXON;
  options.c_oflag = 0;
  options.c_lflag = 0;
  tcflush(uart0_filestream, TCIFLUSH);
  tcsetattr(uart0_filestream, TCSANOW, &options);

  return uart0_filestream;
}


/**
 * Read uart stream into char array
 * uart0_filestream = id of the uart stream open
 * readLength = length of chars to be read
 * readData = char array that is read
 * Return: length of actual read chars
 */
int readUart(int uart0_filestream, int readLength, unsigned char* readData){
  // initialize
  memset(readData, 0x00, readLength);

  if (uart0_filestream == -1){
    return 0;
  } else {
    int rx_length = read(uart0_filestream, (void*)readData, readLength);
    if (rx_length <= 0){
      return 0;
    } else {
      // bytes received
      return rx_length;
    }
  }
}


/**
 * Copy buffer to memory structure
 * Increase size as required
 */
void store_to_memory_struct(struct memory_struct *outData, const uint8_t *cmd_buffer, const size_t cmd_buffer_len){
  if ((outData->size - outData->used_size) < cmd_buffer_len){
    const size_t INCR_CHUNK = 50;
    size_t size_to_incr = INCR_CHUNK;

    // need to increase size
    if (cmd_buffer_len > INCR_CHUNK)
      size_to_incr = cmd_buffer_len;

    increaseMemoryStruct(outData, size_to_incr);
  }

  // copy to memory struct
  memcpy(outData->memory+outData->used_size, cmd_buffer, cmd_buffer_len);
  outData->used_size += cmd_buffer_len;
}


/**
 * This is the comm independent function for executing a duplex write/read operation (transmit data, then wait for response)
 * This function assumes that the port comm is already opened!
 * Returns the number of bytes read as response
 */
size_t duplex_transfer(enum escpos_comm_channel comm, uint8_t *tx, const size_t txSize, uint8_t *rx, const size_t maxRxSize, const size_t timeout){
  size_t rxSize = 0, receivedLen = 0;

  // commence timer
  struct timespec start;
  clock_gettime(CLOCK_REALTIME, &start);

  if (comm==UART){
    // UART
    write(uart_stream, tx, txSize);

    // get response
    do {
      receivedLen = readUart(uart_stream, maxRxSize-rxSize, rx+rxSize);
      rxSize += receivedLen;
    } while (rxSize < maxRxSize && getElapsedTime(start) <= timeout);
  } else {
    // SPI
    spi2uart_write(tx, txSize, timeout);

    // get response
    do {
      receivedLen = spi2uart_read(rx+rxSize, maxRxSize-rxSize);
      rxSize += receivedLen;
    } while (rxSize < maxRxSize && getElapsedTime(start) <= timeout);
  }

  return rxSize;
}


/**
 * Decipher the command that's stored in the cmd_buffer
 * Generate the tx response if appropriate, the number of bytes stored in tx_len
 * If appropriate, it will also store the bytes to pass through in bytes_to_passthru. These are on top of the bytes already in the cmd_buffer
 * Return the command state
 */
enum COMMAND_STATE decipher_command(const uint8_t *cmd_buffer, const size_t cmd_buffer_len, uint8_t *tx, size_t *tx_len, const size_t max_tx_len, size_t *bytes_to_passthru){
  // initialize
  memset(tx, 0x00, max_tx_len);
  *tx_len = 0;
  *bytes_to_passthru = 0;

  if (cmd_buffer[0] == 0x00){
    // if the beginning of the cmd is the NULL char, then ignore
    return IGNORE;
  } else if (cmd_buffer[0]!=GS && cmd_buffer[0]!=DLE && cmd_buffer[0]!=ESC){
    // if the beginning of the cmd is not a special char, then it's just plain text
    if (pc_connected == 0) {
      return PASS_THRU;
    } else {
      return IGNORE;
    }
  }

  // recognized commands
  if (cmd_buffer_len>=4 && cmd_buffer[0]==GS && cmd_buffer[1]=='v' && cmd_buffer[2]=='0' && cmd_buffer[3]==0x00){
    // GS v 0 command, print raster bit image

    fprintf(stderr, "GS v 0");

    if (cmd_buffer_len == 8){
      uint8_t xL = cmd_buffer[4], xH = cmd_buffer[5], yL = cmd_buffer[6], yH = cmd_buffer[7];

      *bytes_to_passthru = (int)(xL+xH*256)*(yL+yH*256);

      fprintf(stderr, " (pass %d bytes)\n", *bytes_to_passthru);
      return PASS_THRU;
    } else {
      *bytes_to_passthru = 0;
      fprintf(stderr, " (more data req)\n");
      return RECOGNIZED;
    }

  } /*else if (cmd_buffer_len==15 && cmd_buffer[0]==GS && cmd_buffer[1]=='(' && cmd_buffer[2]=='L' && cmd_buffer[5]==0x30 && cmd_buffer[6]==0x71 && cmd_buffer[7]==0x30){
    // GS ( L command, store graphics data in print buffer in column format

    uint8_t xL = cmd_buffer[11], xH = cmd_buffer[12], yL = cmd_buffer[13], yH = cmd_buffer[14];
    *bytes_to_passthru = (xL + xH*256)*((int)(((yL + yH*256) + 7)/8));

    return PASS_THRU;

  } else if (cmd_buffer_len==15 && cmd_buffer[0]==GS && cmd_buffer[1]=='(' && cmd_buffer[2]=='L' && cmd_buffer[5]==0x30 && cmd_buffer[6]==0x70){
    // GS ( L command, store graphics data in print buffer in raster format

    uint8_t xL = cmd_buffer[11], xH = cmd_buffer[12], yL = cmd_buffer[13], yH = cmd_buffer[14];
    *bytes_to_passthru = ((int)(((xL + xH*256) + 7)/8))*(yL + yH*256);

    return PASS_THRU;
  } else if (cmd_buffer_len==16 && cmd_buffer[0]==GS && cmd_buffer[1]=='8' && cmd_buffer[2]=='L' && cmd_buffer[7]==0x30 && cmd_buffer[8]==0x71 && cmd_buffer[9]==0x30){
    // GS 8 L command, store graphics data in print buffer in column format

    uint8_t xL = cmd_buffer[13], xH = cmd_buffer[14], yL = cmd_buffer[15], yH = cmd_buffer[16];
    *bytes_to_passthru = ((int)(((xL + xH*256) + 7)/8))*(yL + yH*256);

    return PASS_THRU;
  } else if (cmd_buffer_len==16 && cmd_buffer[0]==GS && cmd_buffer[1]=='8' && cmd_buffer[2]=='L' && cmd_buffer[7]==0x30 && cmd_buffer[8]==0x70){
    // GS 8 L command, store graphics data in print buffer in raster format

    uint8_t xL = cmd_buffer[13], xH = cmd_buffer[14], yL = cmd_buffer[15], yH = cmd_buffer[16];
    *bytes_to_passthru = ((int)(((xL + xH*256) + 7)/8))*(yL + yH*256);

    return PASS_THRU;
  } else if (cmd_buffer_len==8 && cmd_buffer[0]==GS && cmd_buffer[1]=='(' && cmd_buffer[2]=='k'){
    // GS ( k command, store QR code data

    uint8_t pL = cmd_buffer[3], pH = cmd_buffer[4];
    *bytes_to_passthru = (pL+pH*256)-3;

    return PASS_THRU;
  } else if (cmd_buffer_len==4 && cmd_buffer[0]==GS && cmd_buffer[1]=='k'){
    // GS k command, print barcode
    // Assumes function B encoding

    *bytes_to_passthru = cmd_buffer[3];

    return PASS_THRU;
  } */else if (cmd_buffer_len==3 && cmd_buffer[0]==ESC && cmd_buffer[1]=='=' && cmd_buffer[2]==0x01){
    // ESC = command, select peripheral device

    fprintf(stderr, "ESC =\n");

    pc_connected = 0;
    return IGNORE;

  } else if (cmd_buffer_len==3 && cmd_buffer[0]==DLE && cmd_buffer[1]==EOT){
    // DLE EOT command

    fprintf(stderr, "DLE EOT\n");

    if (cmd_buffer[2]==0x01){
      tx[0] = 0x16;
      *tx_len = 1;
    } else if (cmd_buffer[2]==0x02 || cmd_buffer[2]==0x03 || cmd_buffer[2]==0x04){
      tx[0] = 0x12;
      *tx_len = 1;
    }

    pc_connected = 0;
    return REALTIME;

  } else if (cmd_buffer_len==3 && cmd_buffer[0]==GS && cmd_buffer[1]=='I'){
    // GS I command, transmit printer ID

    fprintf(stderr, "GS I\n");

    if (cmd_buffer[2] == 0x01){
      // printer model
      tx[0] = 0x20;
      *tx_len = 1;
    } else if (cmd_buffer[2] == 0x43){
      // printer ID
      tx[0] = '_';
      tx[1] = 'T';
      tx[2] = 'M';
      tx[3] = '-';
      tx[4] = 'T';
      tx[5] = '8';
      tx[6] = '8';
      tx[7] = 'I';
      tx[8] = 'I';
      tx[9] = 'I';
      tx[10] = '\0';
      *tx_len = 11;
    } else if (cmd_buffer[2] == 0x45){
      // printer information B
      tx[0] = 0x5F;
      tx[1] = 0x00;
      *tx_len = 2;
    }

    pc_connected = 0;
    return REALTIME;

  } else if (cmd_buffer_len==3 && cmd_buffer[0]==GS && cmd_buffer[1]=='a'){
    // GS a, basic ASB configuration

    fprintf(stderr, "GS a\n");

    tx[0] = 0x14;
    tx[1] = 0x00;
    tx[2] = 0x00;
    tx[3] = 0x0F;
    *tx_len = 4;

    pc_connected = 0;
    return REALTIME;

  } else if (cmd_buffer_len==4 && cmd_buffer[0]==DLE && cmd_buffer[1]==DC4 && cmd_buffer[2]==0x07){
    // DLE DC4 fn=7, transmit specified status in real time

    fprintf(stderr, "DLE DC4 0x07\n");

    pc_connected = 0;
    if (cmd_buffer[3]==0x01){
      tx[0] = 0x14;
      tx[1] = 0x00;
      tx[2] = 0x00;
      tx[3] = 0x0F;
      *tx_len = 4;

      return REALTIME;
    } else {
      return IGNORE;
    }

  } else if (cmd_buffer_len==3 && cmd_buffer[0]==GS && cmd_buffer[1]=='r'){
    // GS r, transmit status

    fprintf(stderr, "GS r\n");
    if (cmd_buffer[2]==0x01){
      // paper sensor status
      tx[0] = 0x00;
      *tx_len = 1;
    } else if (cmd_buffer[2]==0x02){
      // drawer kickout
      tx[0] = 0x00;
      *tx_len = 1;
    } else if (cmd_buffer[2]==0x04){
      // ink status
      tx[0] = 0x00;
      *tx_len = 1;
    }

    pc_connected = 0;
    return REALTIME;

  } else {
    return UNDECIDED;
  }
}


/**
 * Respond to commands from PC
 * Will store useful receipt data into the data pointer
 * Setting timeout to 0 blocks forever
 * Return: -1 = failed to communicate, 0 = good communication, 1 = successful exit
 */
int escpos_pc_communicate(enum escpos_comm_channel comm, struct memory_struct *outData){
  // buffered received command
  // This buffer is used to decipher the command prior to responding to it or storing it for output
  const size_t max_cmd_buffer_len = 20; uint8_t cmd_buffer[max_cmd_buffer_len]; memset(cmd_buffer, 0x00, max_cmd_buffer_len);
  size_t cmd_buffer_len = 0;

  // command state
  enum COMMAND_STATE cmd_state = UNDECIDED;

  // set timeout for when no data arrives
  size_t timeout = 500;
  size_t elapsedTime = 0;
  struct timespec start;
  clock_gettime(CLOCK_REALTIME, &start);

  // max rx and tx sizes
  const size_t max_rx_len = 1;
  const size_t max_tx_len = 20;
  size_t additional_bytes_passthru = 0;

  // uart stream
  //int uart_stream = -1;

  // This loop will block forever unless there is no more incoming RX
  do {
    // get data
    size_t receivedDataLen = 0;
    uint8_t rxData[max_rx_len]; memset(rxData, 0x00, max_rx_len);

    if (comm == UART){
      // uart selected

      // open uart stream if not already opened
      uart_stream = uart_stream==-1 ? openUart() : uart_stream;
      if (uart_stream == -1)
        return -1;

      // read from stream
      receivedDataLen = readUart(uart_stream, max_rx_len, rxData);

    } else {
      // SPI selected

      // open SPI channel if not already opened
      if (spi2uart_init((comm==SPI0 ? 0:1)) < 0)
        return -1;

      // read from spi2uart bridge
      receivedDataLen = spi2uart_read(rxData, max_rx_len);
    }

    // If nothing is received, begin timeout countdown
    if (receivedDataLen > 0) {

      if (additional_bytes_passthru > 0) {

        // straight pass through to the output
        store_to_memory_struct(outData, rxData, receivedDataLen);
        additional_bytes_passthru -= receivedDataLen;

      } else {

        // If a special command char arrives and is preceded by another command that is marked as UNDECIDED, then
        // we should send the previous command
        // Also,
        // Check if cmd_buffer overflows, if so, then
        // we have no choice but to pass the original content to outData
        if ((cmd_buffer_len>0 && cmd_state==UNDECIDED && (rxData[0]==DLE || rxData[0]==ESC || rxData[0]==GS))
          || (cmd_buffer_len + receivedDataLen) > max_cmd_buffer_len){

          // pass everything to outData
          store_to_memory_struct(outData, cmd_buffer, cmd_buffer_len);

          // reset the cmd_buffer
          memset(cmd_buffer, 0x00, max_cmd_buffer_len);
          cmd_buffer_len = 0;
        }

        // Copy received data to cmd_buffer
        memcpy(cmd_buffer+cmd_buffer_len, rxData, receivedDataLen);
        cmd_buffer_len += receivedDataLen;


        // decipher the command
        uint8_t txData[max_tx_len]; memset(txData, 0x00, max_tx_len);
        size_t tx_len = 0;
        cmd_state = decipher_command(cmd_buffer, cmd_buffer_len, txData, &tx_len, max_tx_len, &additional_bytes_passthru);

        switch (cmd_state){
        case PASS_THRU:
          // transfer the buffer content to outData
          store_to_memory_struct(outData, cmd_buffer, cmd_buffer_len);

          // reset the cmd_buffer
          memset(cmd_buffer, 0x00, max_cmd_buffer_len);
          cmd_buffer_len = 0;
          // reset the cmd_state
          cmd_state = UNDECIDED;

          break;

        case REALTIME:
          // requires a tx response

          if (comm==UART){
            // UART
            write(uart_stream, txData, tx_len);
          } else {
            // SPI
            spi2uart_write(txData, tx_len, timeout);
          }

          // reset the cmd_buffer
          memset(cmd_buffer, 0x00, max_cmd_buffer_len);
          cmd_buffer_len = 0;
          // reset the cmd_state
          cmd_state = UNDECIDED;

          break;

        case IGNORE:
          // throw away the buffer content
          memset(cmd_buffer, 0x00, max_cmd_buffer_len);
          cmd_buffer_len = 0;
          // reset the cmd_state
          cmd_state = UNDECIDED;

          break;
        }
      }


      // refresh the clock
      clock_gettime(CLOCK_REALTIME, &start);
    }
  } while (getElapsedTime(start) <= timeout);


  // close UART or SPI
  /*if (comm == UART){
    // UART
    close(uart_stream);
  } else {
    // SPI
    spi2uart_end();
  }*/
}


/**
 * Connect with the printer 
 * Return: -1 = failure, 0 = connection succeeded
 */
int escpos_printer_connect(enum escpos_comm_channel comm, size_t timeout){
  return 0;
}


/**
 * Get status of the printer 
 */
int escpos_printer_status(enum escpos_comm_channel comm, size_t timeout){
  const size_t MAX_TX_SIZE = 10; uint8_t tx[MAX_TX_SIZE]; memset(tx, 0x00, MAX_TX_SIZE);
  size_t txSize = 0;
  const size_t MAX_RX_SIZE = 10; uint8_t rx[MAX_RX_SIZE]; memset(rx, 0x00, MAX_RX_SIZE);
  size_t rxSize = 0;

  // initialize the comm port if not already initialized
  if (comm == UART){
    // uart selected
    uart_stream = uart_stream==-1 ? openUart() : uart_stream;
    if (uart_stream == -1)
      return -1;

    // flush uart input
    tcflush(uart_stream, TCIFLUSH);
  } else {
    // SPI selected
    if (spi2uart_init((comm==SPI0 ? 0:1)) < 0)
      return -1;
  }

  // DLE EOT printer status
  tx[0] = DLE;
  tx[1] = EOT;
  tx[2] = 0x01;
  txSize = 3;
  // write and get response
  rxSize = duplex_transfer(comm, tx, txSize, rx, 1, timeout);
  // decipher response
  if (rxSize < 1 || rx[0] != 0x16){
    return -1;
  }

  // GS a ASB status
  tx[0] = GS;
  tx[1] = 'a';
  tx[2] = 0xFF;
  txSize = 3;
  // write and get response
  rxSize = duplex_transfer(comm, tx, txSize, rx, 4, timeout);
  // decipher response
  if (rxSize < 4 || rx[0] != 0x14 || rx[1] != 0x00 || rx[2] & 0x0C != 0x00){
    return -1;
  }

  return 0;
}


/**
 * Transfer texts or graphics to the printer 
 * Return: -1 = failure, 0 = success
 */
int escpos_printer_transfer(enum escpos_comm_channel comm, struct memory_struct *data, size_t timeout){
  // select printer command
#define PRE_TX_LEN 3
  uint8_t pre_tx[PRE_TX_LEN] = {ESC, '=', 0x01}; 

  // initialize the comm port if not already initialized
  if (comm == UART){
    // uart selected
    uart_stream = uart_stream==-1 ? openUart() : uart_stream;
    if (uart_stream == -1)
      return -1;

    // transmit
    write(uart_stream, pre_tx, PRE_TX_LEN);
    write(uart_stream, data->memory, data->used_size);
  } else {
    // SPI selected
    if (spi2uart_init((comm==SPI0 ? 0:1)) < 0)
      return -1;

    // transmit
    spi2uart_write(pre_tx, PRE_TX_LEN, timeout);
    spi2uart_write(data->memory, data->used_size, timeout);
  }

  return 0;
}

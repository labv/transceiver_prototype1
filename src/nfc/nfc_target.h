#ifndef __NFC_APP__
#define __NFC_APP__

#include "iso7816.h"

#define MAX_COMMAND_LEN ISO7816_SHORT_C_APDU_MAX_LEN
#define MAX_RESPONSE_LEN ISO7816_SHORT_R_APDU_MAX_LEN
#define NUL_CHAR 0x00

#define ANDROID_APP_NAME "com.labv.receipt"
#define ANDROID_APP_NAME_LEN = 16;

#define USER_ID_LEN 25
char customer_user_id[USER_ID_LEN];

/**
 * Initialize NFC
 * On success, return 0, otherwise, return -1
 */
int nfcTargetInit();


/**
 * Close NFC
 */
void nfcTargetClose();


/**
 * Wrapper function for receiving commands (C-APDU)
 * Returns the number of bytes received. 
 * Returns -1 on failure.
 * Stores the received command in command 
 */
int nfcTargetReceiveCommand(uint8_t* command, int timeout);


/**
 * Wrapper function for emitting the response (R-APDU)
 * Returns 0 on success. Returns -1 on failure.
 */
int nfcTargetTransferResponse(uint8_t* response, size_t response_len, int timeout);


/**
 * Based on the command received (C-APDU), decipher what response should be sent out.
 * Const string appName and msg are the transceiver specific data, they will be parsed
 * by the function as the appropriate response if appropriate command has been detected.
 *
 * Returns the number of bytes that should be transferred as response, and stores into responseOut
 * Returns -1 on failure
 * When termination_success = 1, the whole transaction is finished and should exit
 */
int nfcTargetDecipherIO(const uint8_t* commandIn, const size_t commandInLen, uint8_t* responseOut, size_t maxResponseLen, int* termination_success);

#endif

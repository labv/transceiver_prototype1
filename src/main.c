// basic system
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

// general IO
#include <bcm2835.h>

// NFC
#include "nfc_target.h"
//#include "nfc_master.h"

// utilities
#include "utils.h"

// ESC/POS printer
#include "escpos.h"

// cURL
#include <curl/curl.h>

// uuid generator
#include <uuid/uuid.h>

// GPIO pins
#define POWER_OUT_PIN    19
#define READY_OUT_PIN    13
#define OVERRIDE_IN_PIN  26

// constants
#define POLLING_DELAY 50         // in ms
#define BLINKING_DELAY 500       // in ms
#define RECEIPT_DATA_CHUNK 256
#define TIMEOUT    30000         // in ms
#define UUID_SIZE 37             // 36 charaters + trailing 0x00

// states
enum state_enum {
  STATE_STANDBY = 1,
  STATE_PRINT = 2,
  STATE_NFC = 3,
  STATE_UPLOAD = 4,
  STATE_ERROR = 5
};

// for development debugging only
#define SKIP_STANDBY 0
#define SKIP_NFC 0
#define SKIP_UPLOAD 0
#define SKIP_PRINT 0


/**
 *  Check for printer bypass interrupt
 *  Return true if bypass active
 */
int updatePrinterBypass(){
  return bcm2835_gpio_lev(OVERRIDE_IN_PIN);
}


/**
 * Callback function when curl receives data via http
 */
size_t curlDataCallback(char *curldata, size_t size, size_t nmemb, void *userdata){
  size_t realsize = size * nmemb;
  struct memory_struct *mem = (struct memory_struct *)userdata;

  // copy data to memory
  size_t memsize = realsize+1;
  increaseMemoryStruct(mem, memsize);
  memcpy(mem->memory, curldata, realsize);
  mem->memory[realsize] = NUL_CHAR;
  mem->used_size = memsize;

  return realsize;
}


/**
 * Callback function for curl response header
 */
size_t curlHeaderCallback(char *curldata, size_t size, size_t nmemb, void *userdata){
  size_t realsize = size*nmemb;
  uint8_t *res = (uint8_t *)userdata;

  if (realsize > 8 && curldata[0]=='H' && curldata[1]=='T' && curldata[2]=='T' && curldata[3]=='P'
    && curldata[4]=='/' && curldata[5]=='1' && curldata[6]=='.' && strstr(curldata, "200") != NULL){
    *res = 1;
  }

  return realsize;
}


/**
 * NFC communication thread
 */
pthread_t nfcThreadId = 0;
void* nfcThread(void* param){
  int* nfcSuccess = (int*)param;

  uint8_t command[MAX_COMMAND_LEN]; memset(command, 0, MAX_COMMAND_LEN);
  uint8_t response[MAX_RESPONSE_LEN]; memset(response, 0, MAX_RESPONSE_LEN);

  while(!(*nfcSuccess)){
    // NFC communication
    int res = nfcTargetReceiveCommand(command, 0);

    if (res > 0){
      res = nfcTargetDecipherIO(command, res, response, MAX_RESPONSE_LEN, nfcSuccess);
      if (res > 0){
        res = nfcTargetTransferResponse(response, res, 0);
      }
    }
  }

  return NULL;
}


/**
  * Main program 
  */
int main(int argc, char *argv[])
{
  /* initialize state machine */
  enum state_enum state = STATE_STANDBY;

  /* initialize gpio */
  bcm2835_init();
  //wiringPiSetupGpio();
  bcm2835_gpio_fsel(POWER_OUT_PIN, BCM2835_GPIO_FSEL_OUTP);  // power LED
  bcm2835_gpio_fsel(READY_OUT_PIN, BCM2835_GPIO_FSEL_OUTP);  // NFC transmission readiness
  bcm2835_gpio_fsel(OVERRIDE_IN_PIN, BCM2835_GPIO_FSEL_INPT);  // printing override switch
  // set power LED to high
  bcm2835_gpio_write(POWER_OUT_PIN, HIGH);

  /* cURL */
  CURL *curl;
  CURLcode curlRes;
  struct curl_slist *header = NULL;
  const size_t HTTP_HEADER_SIZE = 100; char header_str[HTTP_HEADER_SIZE];
  curl_global_init(CURL_GLOBAL_DEFAULT);

  /* actual receipt data */
  struct memory_struct receiptData;
  receiptData.size = 0;
  receiptData.used_size = 0;
  receiptData.memory = NULL;

  /* timer */
  struct timespec start_time;
  int time_diff = 0;

  /* read config file */
  char device_uuid[UUID_SIZE]; memset(device_uuid, 0x00, UUID_SIZE);
  char server_key[UUID_SIZE]; memset(server_key, 0x00, UUID_SIZE);
  char server_token[UUID_SIZE]; memset(server_token, 0x00, UUID_SIZE);
  char retailer_id[UUID_SIZE]; memset(retailer_id, 0x00, UUID_SIZE);
  const size_t UPLOAD_ADDRESS_SIZE = 100;
  char upload_address[UPLOAD_ADDRESS_SIZE]; memset(upload_address, 0x00, UPLOAD_ADDRESS_SIZE);

  FILE *fp = fopen("/etc/labv/config","r");
  if (fp == NULL){
    state = STATE_ERROR;
    printf("Cannot open config file\n");
  } else{
    const size_t field_name_len = 30; char field_name[field_name_len];
    const size_t data_len = 100; char data[data_len];
    while (fscanf(fp, "%s = %s", field_name, data) == 2){
      char field_name_trimmed[field_name_len];
      char data_trimmed[data_len];
      strtrim(field_name, field_name_trimmed, field_name_len);
      strtrim(data, data_trimmed, data_len);

      if (strcmp(field_name_trimmed, "device_uuid") == 0)
        strcpy(device_uuid, data_trimmed);
      else if (strcmp(field_name_trimmed, "server_key") == 0)
        strcpy(server_key, data_trimmed);
      else if (strcmp(field_name_trimmed, "server_token") == 0)
        strcpy(server_token, data_trimmed);
      else if (strcmp(field_name_trimmed, "retailer_id") == 0)
        strcpy(retailer_id, data_trimmed);
      else if (strcmp(field_name_trimmed, "upload_receipt") == 0)
        strcpy(upload_address, data_trimmed);
    }

    fclose(fp);
  }

  /* main loop */
  while(1){
    switch (state){
      case STATE_STANDBY:
        // state: standby
        // continuously check for data being transmitted from PC via uart

        printf("STATE Standby\n");

        // reset receiptData
        deleteMemoryStruct(&receiptData);

        // reset customer_user_id
        // this variable is declared in nfc_target.h
        strcpy(customer_user_id, "");

        // set LED to off
        bcm2835_gpio_write(READY_OUT_PIN, LOW);

#if SKIP_STANDBY == 0
        // communicate with the PC/POS and get receipt data
        do {
          int res = escpos_pc_communicate(UART, &receiptData);

          if (res < 0){
            state = STATE_ERROR;
            break;
          }
        } while (receiptData.used_size <= 0);

#else
        bcm2835_delay(2000);

        increaseMemoryStruct(&receiptData, 38);
        strcpy(receiptData.memory, "Sample receipt: everything's awesome");
        receiptData.used_size = 38;
#endif

        if (state != STATE_ERROR){
          // output receipt data to the console
          int p=0;
          fprintf(stderr, "New receipt size = %d\n", receiptData.used_size);
          /*for (p=0; p<receiptData.used_size; p++){
          fprintf(stderr, "0x%02x ", receiptData.memory[p], receiptData.memory[p]);
          }
          fprintf(stderr, "\n");*/

          // determine which state to advance
          if (updatePrinterBypass()){
            state = STATE_PRINT;
            bcm2835_gpio_write(READY_OUT_PIN, LOW);
          } else {
            state = STATE_NFC;
            bcm2835_gpio_write(READY_OUT_PIN, HIGH);
          }
        }

        break;

      case STATE_PRINT:
        // state: send receipt data to printer

        printf("STATE Print Receipt\n");

        // light up the ready LED
        bcm2835_gpio_write(READY_OUT_PIN, HIGH);

#if SKIP_PRINT == 0

        // attempt to communicate with the printer and send the receipt for print
        if (escpos_printer_connect(SPI1, TIMEOUT) < 0 || escpos_printer_transfer(SPI1, &receiptData, TIMEOUT) < 0){
          // unable to connect to the printer, or printer returns erroneous status
          state = STATE_ERROR;
          break;  // exit the switch statement
        }
#else
        bcm2835_delay(3000);
#endif

        // transition state
        if (state != STATE_ERROR)
          state = STATE_STANDBY;

        break;

      case STATE_NFC:
        // state: NFC communication with Android via PN532

        printf("STATE NFC Communication with Android\n");

        // light up the ready LED
        //bcm2835_gpio_write(READY_OUT_PIN, HIGH);

#if SKIP_NFC == 0
        // get current time, start timeout timer
        clock_gettime(CLOCK_REALTIME, &start_time);
        time_diff = 0;

        // initialize NFC
        int nfcSuccess = 0;
        if (nfcTargetInit() < 0){
          state = STATE_ERROR;
          break;
        }

        // create thread
        if (pthread_create(&nfcThreadId, NULL, &nfcThread, &nfcSuccess) != 0){
          state = STATE_ERROR;
          break;
        }

        while(!nfcSuccess) {
          // check for timeout
          // otherwise, blink the ready LED for prompt
          time_diff = getElapsedTime(start_time);

          // check for bypass switch
          // otherwise, check for timeout
          if (updatePrinterBypass())
            break;
          else if (time_diff >= TIMEOUT){
            // timeout has expired, revert to standby state
            break;
          } else if (time_diff % (BLINKING_DELAY*2) >= BLINKING_DELAY){
            bcm2835_gpio_write(READY_OUT_PIN, HIGH);
          } else {
            bcm2835_gpio_write(READY_OUT_PIN, LOW);
          }
        }

        // close NFC
        if (!nfcSuccess)
          pthread_cancel(nfcThreadId);
        else{
          void* thrRes;
          pthread_join(nfcThreadId, &thrRes);
        }
        nfcTargetClose();

        // state transition
        if (nfcSuccess)
          state = STATE_UPLOAD;
        else
          state = STATE_PRINT;

#else

        int totalCount = 0;
        while (1){
          if (updatePrinterBypass()){
            state = STATE_PRINT;
            //bcm2835_gpio_write(READY_OUT_PIN, LOW);
            break;
          } else if (totalCount >= 5000){
            state = STATE_UPLOAD;
            //bcm2835_gpio_write(READY_OUT_PIN, HIGH);
            break;
          }

          // wait for NFC
          bcm2835_delay(POLLING_DELAY);
          totalCount += POLLING_DELAY;
          //bcm2835_gpio_write(READY_OUT_PIN, HIGH);
          // blink transmission ready LED
          if (totalCount % (BLINKING_DELAY*2) == 0){
            bcm2835_gpio_write(READY_OUT_PIN, HIGH);
          } else if (totalCount % BLINKING_DELAY == 0){
            bcm2835_gpio_write(READY_OUT_PIN, LOW);
          }
        }

        // customer_user_id, this field would've otherwise been filled up by nfcTargetDecipherIO()
        strcpy(customer_user_id, "5535188a64657642f4010000");

#endif

        // clear time
        time_diff = 0;

        break;

      case STATE_UPLOAD:
        // state: upload receipt to web

        fprintf(stderr, "STATE Upload Receipt to Server\n");
        bcm2835_gpio_write(READY_OUT_PIN, HIGH);

#if SKIP_UPLOAD == 0
        // initialize curl
        curl = curl_easy_init();
        if (!curl){
          state = STATE_ERROR;
          break;
        }

        // receipt uuid generation
        uuid_t receipt_uuid;
        char receipt_uuid_str[UUID_SIZE]; memset(receipt_uuid_str, 0x00, UUID_SIZE);
        uuid_generate(receipt_uuid);
        uuid_unparse(receipt_uuid, receipt_uuid_str);
        char receipt_uuid_str_nodash[UUID_SIZE];
        strRemoveDash(receipt_uuid_str, receipt_uuid_str_nodash, UUID_SIZE);

        // server response
        /*struct memory_struct server_res;
        server_res.memory = NULL;
        server_res.size = 0;*/
        uint8_t res_ok = 0;

        // follow redirect
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        // set link
        curl_easy_setopt(curl, CURLOPT_URL, upload_address);
        // authorization header
        memset(header_str, 0x00, HTTP_HEADER_SIZE);
        sprintf(header_str, "Authorization: TransceiverAuth uid=%s, sid=%s, token=%s", device_uuid, server_key, server_token);
        header = curl_slist_append(header, header_str);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header);
        // post fields
        struct curl_httppost* post = NULL;
        struct curl_httppost* last = NULL;
        curl_formadd(&post, &last, CURLFORM_COPYNAME, "user_id", CURLFORM_PTRCONTENTS, customer_user_id, CURLFORM_END);
        curl_formadd(&post, &last, CURLFORM_COPYNAME, "retailer_id", CURLFORM_PTRCONTENTS, retailer_id, CURLFORM_END);
        curl_formadd(&post, &last, CURLFORM_COPYNAME, "receipt_client_id", CURLFORM_PTRCONTENTS, receipt_uuid_str_nodash, CURLFORM_END);
        curl_formadd(&post, &last, CURLFORM_COPYNAME, "receipt_content", CURLFORM_PTRCONTENTS, receiptData.memory, CURLFORM_CONTENTSLENGTH, receiptData.used_size, CURLFORM_END);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
        // write data callback
        //curl_easy_setopt(curl, CURLOPT_HEADER, 1);
        //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlDataCallback);
        //curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)(&server_res));
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, curlHeaderCallback);
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, (void*)(&res_ok));

        // perform curl in a blocking manner
        curlRes = curl_easy_perform(curl);
        // clean up curl
        curl_easy_cleanup(curl);
        curl_formfree(post);
        curl_slist_free_all(header); header=NULL;

        // output server response
        //printf("server res (%d) = %s\n", server_res.used_size, server_res.memory);
        printf("server res = %d\n", res_ok);
        if (res_ok == 0){
          state = STATE_ERROR;
          break;
        }

        // clean up server response
        //deleteMemoryStruct(&server_res);
#else
        bcm2835_delay(2000);
        bcm2835_gpio_write(READY_OUT_PIN, LOW);
#endif

        // transition state
        state = STATE_STANDBY;

        break;

      case STATE_ERROR:
        // state: unknown error has occurred

        printf("STATE Unknown error\n");

        // forever stay in this loop
        // blink the power indicator LED
        while(1){
          bcm2835_gpio_write(POWER_OUT_PIN, LOW);
          bcm2835_delay(500);
          bcm2835_gpio_write(POWER_OUT_PIN, HIGH);
          bcm2835_delay(500);
        }

        break;
    }
  }
}

CC = gcc
LIB_COMPILE = -c -fPIC
CFLAGS=-I

# paths creation
BUILD_PATH = ./build
INCLUDE_PATH = ./include
LIB_PATH = ./lib
BIN_PATH = ./bin
SRC_PATH = ./src
CONFIG_PATH = /etc/labv
init:
	mkdir -p $(BUILD_PATH)
	mkdir -p $(INCLUDE_PATH)
	mkdir -p $(LIB_PATH)
	mkdir -p $(BIN_PATH)
	mkdir -p $(CONFIG_PATH)
	cp config $(CONFIG_PATH)

# program level utilities
util: init
	cp $(SRC_PATH)/utils.h $(INCLUDE_PATH)
	cp $(SRC_PATH)/SC16IS750.h $(INCLUDE_PATH)
	cp $(SRC_PATH)/escpos.h $(INCLUDE_PATH)
	$(CC) $(LIB_COMPILE) -o $(LIB_PATH)/utils.o $(SRC_PATH)/utils.c
	$(CC) $(LIB_COMPILE) -o $(LIB_PATH)/SC16IS750.o $(SRC_PATH)/SC16IS750.c
	$(CC) $(LIB_COMPILE) -o $(LIB_PATH)/escpos.o $(SRC_PATH)/escpos.c

# NFC related libraries
NFC_PATH = $(SRC_PATH)/nfc
LIBNFC_PATH = $(SRC_PATH)/third_party/libnfc/libnfc
libnfc: init
	cp $(LIBNFC_PATH)/libnfc/iso7816.h $(INCLUDE_PATH)
	cp $(LIBNFC_PATH)/utils/nfc-utils.h $(INCLUDE_PATH)
	$(CC) $(LIB_COMPILE) -o $(LIB_PATH)/nfc-utils.o $(LIBNFC_PATH)/utils/nfc-utils.c
ndef: init
	cp $(NFC_PATH)/ndef.h $(INCLUDE_PATH)
	$(CC) $(LIB_COMPILE) -o $(LIB_PATH)/ndef.o $(NFC_PATH)/ndef.c

# main program
main: init libnfc ndef util
	cp $(SRC_PATH)/nfc/nfc_target.h $(INCLUDE_PATH)
	$(CC) -o $(BIN_PATH)/main.o $(NFC_PATH)/nfc_target.c $(SRC_PATH)/main.c -lpthread -lcurl -luuid -lrt -lnfc -lbcm2835 $(LIB_PATH)/*.o -I $(INCLUDE_PATH)
	@echo "\nBuild complete\n"
# overall builds
all: init libnfc ndef util main

clean:
	rm -r -f $(BUILD_PATH)
	rm -r -f $(INCLUDE_PATH)
	rm -r -f $(LIB_PATH)
	rm -r -f $(BIN_PATH)

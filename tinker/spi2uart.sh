echo "reset"
spincl -ib -m0 -c8 -s1 -p0 2 0x70 0x08
echo "read scratch"
spincl -m0 -c8 -s1 -p0 2 0xB8 0xff
echo "set scratch to 0x30"
spincl -m0 -c8 -s1 -p0 2 0x38 0x30
echo "read scratch"
spincl -m0 -c8 -s1 -p0 2 0xB8 0xff

echo "read MCR"
spincl -m0 -c8 -s1 -p0 2 0xA0 0xff

echo "Set baud rate"
echo "read LCR"
spincl -m0 -c8 -s1 -p0 2 0x98 0xff
echo "enable divisor register"
spincl -m0 -c8 -s1 -p0 2 0x18 0x80
echo "read LCR"
spincl -m0 -c8 -s1 -p0 2 0x98 0xff
echo "set baud to 38400"
echo "set DLL = least sig byte"
spincl -m0 -c8 -s1 -p0 2 0x00 0x18
echo "set DLH = most sig byte"
spincl -m0 -c8 -s1 -p0 2 0x08 0x00
echo "disable divisor register"
spincl -m0 -c8 -s1 -p0 2 0x18 0x00

echo "Set flow control"
echo "read LCR"
spincl -m0 -c8 -s1 -p0 2 0x98 0xff
echo "enable EFR"
spincl -m0 -c8 -s1 -p0 2 0x18 0xBF
echo "read EFR"
spincl -m0 -c8 -s1 -p0 2 0x90 0xff
echo "set EFR to transmit and receive xon1 xoff1"
spincl -m0 -c8 -s1 -p0 2 0x10 0x0A
echo "read xon1 char"
spincl -m0 -c8 -s1 -p0 2 0xA0 0xff
echo "set xon1 to 0x11"
spincl -m0 -c8 -s1 -p0 2 0x20 0x11
echo "read xoff1 char"
spincl -m0 -c8 -s1 -p0 2 0xB0 0xff
echo "set xoff1 to 0x13"
spincl -m0 -c8 -s1 -p0 2 0x30 0x13
echo "disable EFR"
spincl -m0 -c8 -s1 -p0 2 0x18 0x00

echo "Set FIFO"
spincl -m0 -c8 -s1 -p0 2 0x10 0x01

echo "Set line control"
echo "Set parity = even, num bits = 8, stop bits = 1"
spincl -m0 -c8 -s1 -p0 2 0x18 0x1B
echo "read LCR"
spincl -m0 -c8 -s1 -p0 2 0x98 0xff

echo "Test read"
echo "read LSR"
spincl -m0 -c8 -s1 -p0 2 0xA8 0xff
echo "read RXLVL"
spincl -m0 -c8 -s1 -p0 2 0xC8 0xff
echo "read RHR"
spincl -m0 -c8 -s1 -p0 2 0x80 0xff

echo "Test transmission"
echo "read TXLVL"
spincl -m0 -c8 -s1 -p0 2 0xC0 0xff
echo "send individual Hello to THR"
spincl -m0 -c8 -s1 -p0 2 0x00 0x48
spincl -m0 -c8 -s1 -p0 2 0x00 0x65
spincl -m0 -c8 -s1 -p0 2 0x00 0x6c
spincl -m0 -c8 -s1 -p0 2 0x00 0x6c
spincl -m0 -c8 -s1 -p0 2 0x00 0x6f
spincl -m0 -c8 -s1 -p0 2 0x00 0x0A
echo "send block Hello to THR"
spincl -m0 -c8 -s1 -p0 7 0x00 0x48 0x65 0x6c 0x6c 0x6f 0x0A
echo "read TXLVL again"
spincl -m0 -c8 -s1 -p0 2 0xC0 0xff

#ifndef _PROG_UTIL
#define _PROG_UTIL

#include <time.h>

/**
 * Free memory of a dynamic array
 */
void deleteArray(char** array);


/**
 * Convenience struct used for memory storage
 */
struct memory_struct {
  char *memory;
  size_t size;        // total size allocated to memory
  size_t used_size;   // size that's been written to
};

/**
 * Increase size of memory_struct by certain amount
 * Return 1 on success, 0 on failure
 */
int increaseMemoryStruct(struct memory_struct* ms, size_t size);

/**
 * Delete memory_struct
 */
void deleteMemoryStruct(struct memory_struct* ms);

/**
 * Trim string
 * Return the new trimmed string
 */
void strtrim(const char* orig_str, char* out, const size_t out_len);

/**
 * Remove dash from the string
 */
void strRemoveDash(const char* orig_str, char* out, const size_t out_len);

/**
 * Get elapsed time in ms since start_time
 */
int getElapsedTime(struct timespec start);

#endif

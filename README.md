# POS Transceiver Device Program (Prototype 1) #


## Introduction ##

This is the program for Prototype 1 of the TreeBox POS transceiver device.  
It has been tested with EPSON TM-T88III. The program is targeted for a Raspberry Pi 2 Model A (2.1) running the Raspbian OS.  


## Pre-requisites ##

1. Requires libnfc 1.7.x  
  git clone https://github.com/nfc-tools/libnfc.git to ./src/third_party/libnfc  
  Generate the configuration and compile as per https://learn.adafruit.com/adafruit-nfc-rfid-on-raspberry-pi/building-libnfc  
  edit ./Makefile to specify the source directory of libnfc that was cloned from github  
  make and sudo make install  

2. Requires BCM2835 Pi library  
  wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.44.tar.gz and tar into ./src/third_party/bcm2835  
  ./configure  
  make and sudo make install

3. Requires libcurl4-openssl-dev  
  sudo apt-get install libcurl4-openssl-dev   

4. Requires uuid-dev
  sudo apt-get install uuid-dev   

## Setup ##

1. Install all prerequisites.
2. Build with 
  sudo ./make all
3. Modify ./config to have the correct values
4. Disable system level UART using raspi-config to free UART
5. sudo nano /etc/modprobe.d/raspi-blacklist.conf  
  Take out all the black-listed devices
6. Run with 
  sudo ./bin/main.o

## Disable Login ##

1. sudo nano /etc/inittab
2. Modify the file with the following lines:  
  #1:2345:respawn:/sbin/getty 115200 tty1  
  1:2345:respawn:/bin/login -f pi tty1 </dev/tty1 >/dev/tty1 2>&1
  
## Start After Login ##

Add the execution of ./bin/main.o into /etc/profile
#include <sys/types.h>
#include <sys/stat.h>

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nfc/nfc.h>
#include "ndef.h"
#include "nfc_comm.h"

// local NFC variables
static nfc_device *pnd;
static nfc_context *context;
static nfc_target nt;
// set NFC modulation
static const nfc_modulation passiveNm = {
  .nmt = NMT_ISO14443A,
  .nbr = NBR_106,
};

static const int NUL_CHAR = 0x00;
static const int EOT = 0x03;
static const int SHORT_R_APDU_SIZE = 258;
static const int DEFAULT_ARRAY_SIZE = 256;

/*
 * Close NFC
 */
void nfcMasterClose(){
  if (pnd != NULL)
    nfc_close(pnd);
  if (context != NULL)
    nfc_exit(context);
}


/**
 * Initialize NFC
 * On success, return 1, otherwise, return 0
 */
int nfcMasterInit(){
  int tries = 0;
  const int totalTries = 5;
  uint8_t abtRx[SHORT_R_APDU_SIZE];

  for (tries=0; tries<totalTries; tries++){
    // initialize libnfc
    nfc_init(&context);
    if (context == NULL){
      fprintf(stderr, "Unable to init libnfc");
      continue;
    }

    // open NFC device
    // should supply the connstring for PN532
    pnd = nfc_open(context, NULL);
    if (pnd == NULL){
      fprintf(stderr, "Unable to open NFC device");
      nfc_exit(context);
      continue;
    }

    // open NFC device to initiator mode
    if (nfc_initiator_init(pnd) < 0){
      fprintf(stderr, "NFC initiator failed");
      nfc_comm_close();
      continue;
    }

    // disable infinite time target selection
    if (nfc_device_set_property_bool(pnd, NP_INFINITE_SELECT, false) < 0){
      fprintf(stderr, "Unable to customize nfc_device");
      nfc_comm_close();
      continue;
    }

    break; // do not allow the last tries++
  }

  if (tries >= totalTries)
    return 0;
  else
    return 1;
}


/**
 * Return 1 if an NFC target is present
 * Return 0 otherwise
 * Stores the resulting NFC target in nt
 */
int nfcDiscoverTarget(){
  /*const nfc_modulation nmTypes[1] = {passiveNm};
  int res;
  res = nfc_initiator_poll_target(pnd, nmTypes, 1, 1, 3, &nt);
  if (res>0)
    fprintf(stderr,"device found\n");
  return res>0;*/

  /*int res;
  res = nfc_initiator_select_dep_target(pnd, NDM_PASSIVE, NBR_106, NULL, &nt, 500);
  fprintf(stderr, "dep res = %d\n",res);
  return res>0;*/

  int res;
  res = nfc_initiator_select_passive_target(pnd, passiveNm, NULL, 0, &nt);
  return res>0;
}


/**
 * Encapsulate dataRecords in an ndef message.
 * dataRecords is an array of char arrays (strings) to be supplied in; each array represents
 * one data record. The number of records should correspond to numRecords
 * The encapsulated data will be stored in outBuffer.
 * Return 1 if success, 0 if failed.
 */
static int buildNdef(char* outBuffer, int outLength, char** dataRecords, int numRecords) {
  int i = 0, recI = 0;
  outBuffer[i++] = NUL_CHAR;

  // begin the process of transcribing records to outBuffer using the NDEF format
  for (recI=0; recI<numRecords; recI++) {
    int recordLength = strlen(dataRecords[recI]) + 1; // to include the termination char

    // parameters:
    // tnf, is_begin, is_end, is_chunk
    // is_short, has_length
    // type, type_length
    // id, id_length
    // payload, payload_length
    struct ndef_record* r = ndef_create(
      WELL_KNOWN, true, false, false,
      true, false,
      NDEF_PAYLOAD_TYPE_URI, 1,
      NULL, 0,
      dataRecords[recI], recordLength
    );

    outBuffer[i++] += r->length;

    // check to make sure the buffer has adequate size
    if (outLength <= 2 + outBuffer[i-1]) {
      return 0;
    }

    // copy payload to buffer
    memcpy(&outBuffer[i], r->buffer, r->length);
    i += r->length;

    // free memory
    ndef_destroy_buffer(r);
  }

  return 1;
}


/**
 * Transmit then receive data from the target
 * Return 1 on success, return 0 on failure
 */
int nfcMasterTransceive(char** txData, int numRecordsTx, struct ndef_message* rxData, int timeout){
  // initialize if things haven't been initialized
  if ((context == NULL || pnd == NULL) && !nfc_comm_init()){
    return 0;
  }

  // encapsualte transmission data with ndef
  const int txSize = DEFAULT_ARRAY_SIZE;
  const int rxSize = DEFAULT_ARRAY_SIZE;

  // ndef buffer initialization
  uint8_t ndef_file[txSize];
  int i=0;
  for (i=0; i<txSize; i++){
    ndef_file[i] = NUL_CHAR;
  }
  if (!buildNdef((char*)ndef_file, txSize, txData, numRecordsTx)){
    fprintf(stderr, "Cannot build ndef");
    return 0;
  }

  // initialize read data
  uint8_t rxDataRaw[rxSize];
  for (i=0; i<rxSize; i++){
    rxDataRaw[i] = 0;
  }

  // construct command APDU
  const uint8_t cmd[] = {0x00, 0xA4, 0x04, 0x00, 0x05, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0x00};
  //uint8_t cmd[txSize+2];
  //memcpy(cmd, "\x90\x00", 2);
  //memcpy(cmd+2, ndef_file, txSize);

  int res = nfc_initiator_transceive_bytes(pnd, cmd, 11, rxDataRaw, rxSize, timeout);
  if (res < 0) {
    fprintf(stderr, "Failed transceive res = %d\n", res);
    return 0;
  }

fprintf(stderr, "success res = %d\n",res);
for (i=0; i<res; i++)
fprintf(stderr, "%x ", rxDataRaw[i]);

  return 1;
}

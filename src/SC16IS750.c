#include <stdio.h>
#include <string.h>
#include <bcm2835.h>

#include "SC16IS750.h"


/* SC16IS750 Register definitions (shifted to align) */
enum RegisterName { 
  // 16750 addresses. Registers accessed when LCR[7] = 0.  
  RHR         = 0x00 << 3, /* Rx buffer register     - Read access  */
  THR         = 0x00 << 3, /* Tx holding register    - Write access */
  IER         = 0x01 << 3, /* Interrupt enable reg   - RD/WR access */
 
  // 16750 addresses. Registers accessed when LCR[7] = 1.
  DLL         = 0x00 << 3, /* Divisor latch (LSB)    - RD/WR access */
  DLH         = 0x01 << 3, /* Divisor latch (MSB)    - RD/WR access */
 
  // 16750 addresses. IIR/FCR is accessed when LCR[7:0] <> 0xBF.
  // Bit 5 of the FCR register is accessed when LCR[7] = 1.
  IIR         = 0x02 << 3, /* Interrupt id. register - Read only    */
  FCR         = 0x02 << 3, /* FIFO control register  - Write only   */

  // 16750 addresses. EFR is accessed when LCR[7:0] = 0xBF. 
  EFR         = 0x02 << 3, /* Enhanced features reg  - RD/WR access */     

  // 16750 addresses.  
  LCR         = 0x03 << 3, /* Line control register  - RD/WR access */

  // 16750 addresses. MCR/LSR is accessed when LCR[7:0] <> 0xBF.
  // Bit 7 of the MCR register is accessed when EFR[4] = 1.     
  MCR         = 0x04 << 3, /* Modem control register - RD/WR access */
  LSR         = 0x05 << 3, /* Line status register   - Read only    */
 
  // 16750 addresses. MSR/SPR is accessed when LCR[7:0] <> 0xBF.
  // MSR, SPR register is accessed when EFR[1]=0 and MCR[2]=0. 
  MSR         = 0x06 << 3, /* Modem status register  - Read only    */
  SPR         = 0x07 << 3, /* Scratchpad register    - RD/WR access */

  // 16750 addresses. TCR/TLR is accessed when LCR[7:0] <> 0xBF.
  // TCR, TLR register is accessed when EFR[1]=1 and MCR[2]=1.
  TCR         = 0x06 << 3, /* Transmission control register - RD/WR access */
  TLR         = 0x07 << 3, /* Trigger level register        - RD/WR access */
 
  // 16750 addresses. XON, XOFF is accessed when LCR[7:0] = 0xBF.
  XON1        = 0x04 << 3, /* XON1 register          - RD/WR access */
  XON2        = 0x05 << 3, /* XON2 register          - RD/WR access */
  XOFF1       = 0x06 << 3, /* XOFF1 register         - RD/WR access */
  XOFF2       = 0x07 << 3, /* XOFF2 register         - RD/WR access */
 
  // 16750 addresses.
  TXLVL       = 0x08 << 3, /* TX FIFO Level register - Read only    */
  RXLVL       = 0x09 << 3, /* RX FIFO Level register - Read only    */
  IODIR       = 0x0A << 3, /* IO Pin Direction reg   - RD/WR access */
  IOSTATE     = 0x0B << 3, /* IO Pin State reg       - RD/WR access */
  IOINTENA    = 0x0C << 3, /* IO Interrupt Enable    - RD/WR access */
  //        reserved    = 0x0D << 3,
  IOCTRL      = 0x0E << 3, /* IO Control register    - RD/WR access */
  EFCR        = 0x0F << 3, /* Extra features reg     - RD/WR access */
};


/* Baud rate */
#define SC16IS750_XTAL_FREQ              14745600UL /* On-board crystal (New mid-2010 Version) */
#define SC16IS750_PRESCALER_1                   1   /* Default prescaler after reset           */
#define SC16IS750_PRESCALER_4                   4   /* Selectable by setting MCR[7]            */
#define SC16IS750_PRESCALER                      SC16IS750_PRESCALER_1  
#define SC16IS750_BAUDRATE_DIVISOR(baud)       ((SC16IS750_XTAL_FREQ/SC16IS750_PRESCALER)/(baud*16UL))


/* Flow control */
enum Flow {
  Disabled = 0,
  RTS,
  CTS,
  RTSCTS,
  XONXOFF
};

/** IO Control register */
#define IOC_SW_RST			(0x04)

/** See section 8.10 of the datasheet for definitions
  * of bits in the Enhanced Features Register (EFR)
  */
#define EFR_ENABLE_CTS                  (1 << 7)
#define EFR_ENABLE_RTS                  (1 << 6)
#define EFR_ENABLE_XOFF2_CHAR_DETECT    (1 << 5)
#define EFR_ENABLE_ENHANCED_FUNCTIONS   (1 << 4)
// EFR[3:0] are used to define Software Flow Control mode
// See section 7.3
#define EFR_DISABLE_TX_FLOW_CTRL        (0x0 << 2)
#define EFR_TX_XON2_XOFF2               (0x1 << 2)
#define EFR_TX_XON1_XOFF1               (0x2 << 2)
#define EFR_TX_XON2_1_XOFF2_1           (0x3 << 2)
 
#define EFR_DISABLE_RX_FLOW_CTRL        (0x0 << 0)
#define EFR_RX_XON2_XOFF2               (0x1 << 0)
#define EFR_RX_XON1_XOFF1               (0x2 << 0)
#define EFR_RX_XON2_1_XOFF2_1           (0x3 << 0)
 
#define EFR_TX_XON2_XOFF2_RX_FLOW       (0x1 << 2) | (0x3 << 0)
#define EFR_TX_XON1_XOFF1_RX_FLOW       (0x2 << 2) | (0x3 << 0)
#define EFR_TX_XON2_1_XOFF2_1_RX_FLOW   (0x3 << 2) | (0x3 << 0)


/** See section 8.4 of the datasheet for definitions
  * of bits in the Line Control Register (LCR)
  */
#define LCR_BITS5                      0x00
#define LCR_BITS6                      0x01
#define LCR_BITS7                      0x02
#define LCR_BITS8                      0x03
 
#define LCR_STOP_BITS1                 0x00
#define LCR_STOP_BITS2                 0x04
 
#define LCR_NONE                       0x00
#define LCR_ODD                        0x08
#define LCR_EVEN                       0x18
#define LCR_FORCED1                    0x28
#define LCR_FORCED0                    0x38
 
#define LCR_BRK_ENA                    0x40
#define LCR_BRK_DIS                    0x00
 
#define LCR_ENABLE_DIV                 0x80
#define LCR_DISABLE_DIV                0x00
 
#define LCR_ENABLE_ENHANCED_FUNCTIONS (0xBF)


/** See section 8.3 of the datasheet for definitions
  * of bits in the FIFO Control Register (FCR)
  */
#define FCR_RX_IRQ_60                 (3 << 6)
#define FCR_RX_IRQ_56                 (2 << 6)
#define FCR_RX_IRQ_16                 (1 << 6)
#define FCR_RX_IRQ_8                  (0 << 6)
//TX Level only accessible when EFR[4] is set
#define FCR_TX_IRQ_56                 (3 << 4)
#define FCR_TX_IRQ_32                 (2 << 4)
#define FCR_TX_IRQ_16                 (1 << 4)
#define FCR_TX_IRQ_8                  (0 << 4)
//#define FCR_RESERVED                  (1 << 3)
#define FCR_TX_FIFO_RST               (1 << 2)
#define FCR_RX_FIFO_RST               (1 << 1)
#define FCR_ENABLE_FIFO               (1 << 0)


/** See section 8.5 of the datasheet for definitions
  * of bits in the Line status register (LSR)
  */
#define LSR_DR   (0x01) /* Data ready in RX FIFO                       */
#define LSR_OE   (0x02) /* Overrun error                               */
#define LSR_PE   (0x04) /* Parity error                                */
#define LSR_FE   (0x08) /* Framing error                               */
#define LSR_BI   (0x10) /* Break interrupt                             */
#define LSR_THRE (0x20) /* Transmitter holding register (FIFO empty)   */
#define LSR_TEMT (0x40) /* Transmitter empty (FIFO and TSR both empty) */
#define LSR_FFE  (0x80) /* At least one PE, FE or BI in FIFO           */


/* The maximum length of block write available on the THR register (64 bytes) */
#define BULK_BLOCK_LEN              16
#define POLLING_DELAY               10

static int spi2uart_stream = -1;

/**
 * Read from a register
 * A register is a 8-bit storage element on the SC16IS750 chip
 * Return the char read
 */
uint8_t read_register(enum RegisterName registerAddress){
  // Used in SPI read operations to flush slave's shift register

  // tx buffer
  uint8_t tx[2];
  tx[0] = registerAddress | 0x80;  // 0x80 is required to access read function
  tx[1] = 0xff;
  // rx buffer
  uint8_t rx[2] = {0x00, 0x00};

  // perform duplex write operation
  bcm2835_spi_transfernb(tx, rx, 2);
  return rx[1];
}


/**
 * Write to a register
 * A register is a 8-bit storage element on the SC16IS750 chip
 */
void write_register(enum RegisterName registerAddress, uint8_t data){
  // tx buffer
  uint8_t tx[2];
  tx[0] = registerAddress; tx[1] = data;

  // write
  bcm2835_spi_transfern(tx, 2);
}


/**
 * Write a block of data to the THR register
 * Assumes that THR has enough space to write
 */
void write_block(uint8_t* data, const size_t len){
  // tx buffer
  uint8_t tx[len+1];
  enum RegisterName reg = THR; tx[0] = reg;
  memcpy(tx+1, data, len);

  // write
  bcm2835_spi_transfern(tx, len+1);
}


/**
 * Set up baud rate
 */
void set_baud(unsigned int baudrate){
  unsigned long divisor = SC16IS750_BAUDRATE_DIVISOR(baudrate);
  char lcr_tmp = 0x00;

  lcr_tmp = read_register(LCR);                            // Read current LCR register
  write_register(LCR, lcr_tmp | LCR_ENABLE_DIV);           // Enable Divisor registers
  write_register(DLL, (divisor & 0xFF));            //   write divisor LSB
  write_register(DLH, ((divisor >> 8) & 0xFF));            //   write divisor MSB
  write_register(LCR, lcr_tmp);                            // Restore LCR register, activate regular RBR, THR and IER registers
}


/**
 * Set up flow control 
 */
void set_flow_control(enum Flow type){
  char lcr_tmp = 0x00, efr_tmp = 0x00;
  
  // We need to enable flow control to prevent overflow of buffers and lose data.
 
  switch (type) {
    case Disabled: 
      break;
    case RTS:
      efr_tmp = EFR_ENABLE_RTS;
      break;     
    case CTS:
      efr_tmp = EFR_ENABLE_CTS;
      break;     
    case RTSCTS:
      efr_tmp = EFR_ENABLE_RTS | EFR_ENABLE_CTS;
      break;
    case XONXOFF:
      efr_tmp = EFR_TX_XON1_XOFF1 | EFR_RX_XON1_XOFF1;
      break;
  }

  // read and save LCR register
  lcr_tmp = read_register(LCR);
  // write magic number 0xBF to enable access to EFR register
  write_register(LCR, LCR_ENABLE_ENHANCED_FUNCTIONS);
  // set flow and enable enhanced functions
  write_register(EFR, efr_tmp);
  // set XON1, XOFF1
  write_register(XON1, 0x11); // DC1 control char
  write_register(XOFF1, 0x13); // DC3 control char
  // restore LCR register
  write_register(LCR, lcr_tmp);
}


/**
 * Set up line control
 */
void set_line_control(size_t parity, size_t stopbits, size_t numbits){
  write_register(LCR, parity | stopbits | numbits);
}


/**
 * Reset
 * Active low 
 */
void spi2uart_reset(){
  write_register(IOCTRL, IOC_SW_RST);
}


/**
 * Set FIFO control (first-in first-out) 
 */
void set_fifo_control(int enable){
  // reset TXFIFO, reset RXFIFO, non FIFO mode  
  write_register(FCR, FCR_TX_FIFO_RST | FCR_RX_FIFO_RST);

  char fifoformat = FCR_RX_IRQ_8 | FCR_TX_IRQ_8;  //Default
  if (enable)
    // enable FIFO mode and set FIFO control values
    write_register(FCR, fifoformat | FCR_ENABLE_FIFO);
  else
    // disable FIFO mode and set FIFO control values  
    write_register(FCR, fifoformat);
 
  // Set Trigger level register TLR for RX and TX interrupt generation
  // Note TLR only accessible when EFR[4] is set (enhanced functions enable) and MCR[2] is set
  //   TRL Trigger levels for RX and TX are 0..15, meaning 0-60 with a granularity of 4 chars    
  // When TLR for RX or TX are 'Zero' the corresponding values in FCR are used. The FCR settings
  // have less resolution (only 4 levels) so TLR is considered an enhanced function.
  write_register(TLR, 0x00);                                     // Use FCR Levels
}


/**
 * Test uart connection
 * Return 1 if connectd, 0 otherwise
 */
int connected(){
  // Perform read/write test on the SPR register (scratch pad) to check if UART is working
  const char TEST_CHARACTER = 'H';
  write_register(SPR, TEST_CHARACTER);
  char a = read_register(SPR);
  return (a == TEST_CHARACTER);
}


/**
 * Test if there is data to be read on the uart bridge 
 * Return 1 if there is, 0 otherwise
 */
int readable(){
  /*if (read_register(LSR) & LSR_DR) { // Data in Receiver Bit, at least one character waiting
    return 1;
  }*/

  uint8_t rxlvl = read_register(RXLVL);

  if (rxlvl > 0)
    return 1;
  else
    return 0;
}


/**
 * Initialize the board 
 * SPI = 0 or 1
 * Return the SPI port opened on success, -1 on failure
 */
int spi2uart_init(int SPI){
  if (spi2uart_stream <= -1){
    // set Pi level SPI
    bcm2835_spi_begin();
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_256);
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
  }

  // set SSEL active low
  if (SPI==1){
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS1, 0);
    bcm2835_spi_chipSelect(BCM2835_SPI_CS1);
  } else {
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, 0);
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
  }

  if (spi2uart_stream <= -1){
    // initialize the spi2uart chip
    spi2uart_reset();
    set_baud(38400);
    set_flow_control(Disabled);
    set_fifo_control(1);
    set_line_control(LCR_EVEN, LCR_STOP_BITS1, LCR_BITS8);
  }

  // test connection
  if (connected()){
    spi2uart_stream = SPI;
    return 0;
  } else {
    spi2uart_stream = -1;
    return -1;
  }
}


/**
 * End the spi2uart and return settings to default
 */
void spi2uart_end(){
  bcm2835_spi_end();
  spi2uart_stream = -1;
}


/**
 * Transfer data to uart
 * Will block until the len amount of bytes have been transferred, unless
 * connection has been severed, or idle time has surpassed TIMEOUT
 * Return the total number of bytes transferred
 */
size_t spi2uart_write(char* data, const size_t len, const unsigned int timeout){
  int totalDelay = 0, res = 0;
  size_t len_w = len;
  
  // Write blocks of BULK_BLOCK_LEN  
  while (len_w > 0) {
    while (read_register(TXLVL) < BULK_BLOCK_LEN) {
      // Wait for space in TX buffer
      bcm2835_delay(POLLING_DELAY);
      totalDelay += POLLING_DELAY;

      if (totalDelay >= timeout)
        return res;
    };

    // determine how many bytes to bulk write
    int writeSize = (len_w >= BULK_BLOCK_LEN) ? BULK_BLOCK_LEN : len_w;
    write_block(data, writeSize);    
    res += writeSize;

    if (len_w >= BULK_BLOCK_LEN){
      len_w  -= BULK_BLOCK_LEN;
      data += BULK_BLOCK_LEN;
    } else
      break;
  }

  return res;
}


/**
 * Read data from uart
 * Will block until len bytes have been read or no more bytes to read
 * Return the number of bytes read
 */
size_t spi2uart_read(char* res, const size_t len){
  if (!readable())
    return 0;

  int count = 0;
  // RHR = Receive Holding Register
  while (readable() && count < len){
    res[count] = read_register(RHR);
    count++;
  }

  return count;
}
